typeset -U path cdpath fpath manpath

source $HOME/.aliases
limit coredumpsize 0

export EDITOR=vim
export TAR_OPTIONS=--warning=no-unknown-keyword

export GOBIN=$HOME/bin
export GOPATH=$HOME/.gopath

export HOMEBREW_NO_AUTO_UPDATE=1
export HOMEBREW_NO_INSTALL_CLEANUP=1
export HOMEBREW_NO_INSTALLED_DEPENDENTS_CHECK=1

# export OBJC_DISABLE_INITIALIZE_FORK_SAFETY=YES

export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

export PERLDOC=-MPod::Text::Color::Delight
# export PERL_CPAN_MIRROR_TINY_BASE=$HOME/.perl-cpm/cache
export PERL_STRICTURES_EXTRA=1
export TEST_PAUSE_PERMISSIONS_METACPAN=1

if [[ $TERM = screen ]]; then
  export TERM=xterm-256color
fi

if [[ -f $HOME/.zshenv.local ]]; then
  source $HOME/.zshenv.local
fi
