# Configuration file for the color ls utility
# Synchronized with coreutils 8.5 dircolors
# This file goes in the /etc directory, and must be world readable.
# You can copy this file to .dir_colors in your $HOME directory to override
# the system defaults.

# COLOR needs one of these arguments: 'tty' colorizes output to ttys, but not
# pipes. 'all' adds color characters to all output. 'none' shuts colorization
# off.
COLOR tty

# Extra command line options for ls go here.
# Basically these ones are:
#  -F = show '/' for dirs, '*' for executables, etc.
#  -T 0 = don't trust tab spacing when formatting ls output.
OPTIONS -F -T 0

# Below, there should be one TERM entry for each termtype that is colorizable
TERM Eterm
TERM ansi
TERM color-xterm
TERM con132x25
TERM con132x30
TERM con132x43
TERM con132x60
TERM con80x25
TERM con80x28
TERM con80x30
TERM con80x43
TERM con80x50
TERM con80x60
TERM cons25
TERM console
TERM cygwin
TERM dtterm
TERM eterm-color
TERM gnome
TERM gnome-256color
TERM jfbterm
TERM konsole
TERM kterm
TERM linux
TERM linux-c
TERM mach-color
TERM mlterm
TERM putty
TERM rxvt
TERM rxvt-256color
TERM rxvt-cygwin
TERM rxvt-cygwin-native
TERM rxvt-unicode
TERM rxvt-unicode256
TERM screen
TERM screen-256color
TERM screen-256color-bce
TERM screen-bce
TERM screen-w
TERM screen.linux
TERM vt100
TERM xterm
TERM xterm-16color
TERM xterm-256color
TERM xterm-88color
TERM xterm-color
TERM xterm-debian

# EIGHTBIT, followed by '1' for on, '0' for off. (8-bit output)
EIGHTBIT 1

# Below are the color init strings for the basic file types. A color init
# string consists of one or more of the following numeric codes:
# Attribute codes:
# 00=none 01=bold 04=underscore 05=blink 07=reverse 08=concealed
# Text color codes:
# 30=black 31=red 32=green 33=yellow 34=blue 35=magenta 36=cyan 37=white
# Background color codes:
# 40=black 41=red 42=green 43=yellow 44=blue 45=magenta 46=cyan 47=white
#NORMAL 00	# no color code at all
#FILE 00	# normal file, use no color at all
RESET 0 # reset to "normal" color
DIR 1;34	# directory
LINK 1;36	# symbolic link (If you set this to 'target' instead of a
		# numerical value, the color is as for the file pointed to.)
MULTIHARDLINK 00	# regular file with more than one link
FIFO 40;33	# pipe
SOCK 1;35	# socket
DOOR 1;35	# door
BLK 40;33;01	# block device driver
CHR 40;33;01	# character device driver
ORPHAN 40;31;01  # symlink to nonexistent file, or non-stat'able file
MISSING 1;05;37;41 # ... and the files they point to
SETUID 37;41	# file that is setuid (u+s)
SETGID 30;43	# file that is setgid (g+s)
CAPABILITY 30;41	# file with capability
STICKY_OTHER_WRITABLE 30;42 # dir that is sticky and other-writable (+t,o+w)
OTHER_WRITABLE 34;42 # dir that is other-writable (o+w) and not sticky
STICKY 37;44	# dir with the sticky bit set (+t) and not other-writable

# This is for files with execute permission:
EXEC 1;32

# List any file extensions like '.gz' or '.tar' that you would like ls
# to colorize below. Put the extension, a space, and the color init string.
# (and any comments you want to add after a '#')
# executables (bright green)
#.cmd 01;32
#.exe 01;32
#.com 01;32
#.btm 01;32
#.bat 01;32
#.sh  01;32
#.csh 01;32
 # archives or compressed (bright red)
.tar 1;31
.tgz 1;31
.arj 1;31
.taz 1;31
.lzh 1;31
.lzma 1;31
.tlz 1;31
.txz 1;31
.zip 1;31
.z   1;31
.Z   1;31
.dz  1;31
.gz  1;31
.lz  1;31
.xz  1;31
.bz2 1;31
.tbz 1;31
.tbz2 1;31
.bz  1;31
.tz  1;31
.deb 1;31
.rpm 1;31
.jar 1;31
.rar 1;31
.ace 1;31
.zoo 1;31
.cpio 1;31
.7z  1;31
.rz  1;31

# image formats (magenta)
.jpg 1;35
.jpeg 1;35
.gif 1;35
.bmp 1;35
.pbm 1;35
.pgm 1;35
.ppm 1;35
.tga 1;35
.xbm 1;35
.xpm 1;35
.tif 1;35
.tiff 1;35
.png 1;35
.svg 1;35
.svgz 1;35
.mng 1;35
.pcx 1;35
.mov 1;35
.mpg 1;35
.mpeg 1;35
.m2v 1;35
.mkv 1;35
.ogm 1;35
.mp4 1;35
.m4v 1;35
.mp4v 1;35
.vob 1;35
.qt  1;35
.nuv 1;35
.wmv 1;35
.asf 1;35
.rm  1;35
.rmvb 1;35
.flc 1;35
.avi 1;35
.fli 1;35
.flv 1;35
.gl 1;35
.dl 1;35
.xcf 1;35
.xwd 1;35
.yuv 1;35
.cgm 1;35
.emf 1;35

# http://wiki.xiph.org/index.php/MIME_Types_and_File_Extensions
.axv 1;35
.anx 1;35
.ogv 1;35
.ogx 1;35

# audio formats (cyan)
.aac 1;36
.au 1;36
.flac 1;36
.mid 1;36
.midi 1;36
.mka 1;36
.mp3 1;36
.mpc 1;36
.ogg 1;36
.ra 1;36
.wav 1;36

# http://wiki.xiph.org/index.php/MIME_Types_and_File_Extensions
.axa 1;36
.oga 1;36
.spx 1;36
.xspf 1;36
