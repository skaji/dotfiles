#!/bin/bash

set -euxo pipefail

yum install -y --allowerasing curl
yum groupinstall -y 'Development Tools'
yum install -y \
  bind-utils \
  bzip2-devel \
  dstat \
  git \
  glibc-langpack-en \
  hostname \
  iproute \
  ncurses-devel \
  net-tools \
  openssl \
  openssl-devel \
  perl \
  perl-core \
  procps \
  readline \
  readline-devel \
  sqlite \
  sqlite-devel \
  sudo \
  vim \
  wget \
  xz \
  zlib-devel \
  zsh \
  ;

ln -sf /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

useradd -G wheel -m -s /bin/zsh skaji
echo skaji:skaji | chpasswd

echo '* soft nofile 1048576' >> /etc/security/limits.conf
echo '* hard nofile 1048576' >> /etc/security/limits.conf

# curl -fsSL https://github.com/krallin/tini/releases/download/v0.19.0/tini -o /sbin/tini
# chmod +x /sbin/tini
