#!/usr/bin/env perl
use v5.16;
use warnings;

package App {
    use Cwd ();
    use File::Basename ();
    use File::Path ();
    use File::Spec;

    sub new {
        my $class = shift;
        my $dir = shift || Cwd::abs_path(File::Basename::dirname($0));
        my $home = (<~>)[0];
        my @path = File::Spec->path;
        my $local_bin = File::Spec->catdir($home, "local", "bin");
        my $home_bin = File::Spec->catdir($home, "bin");
        unshift @path, $home_bin;
        unshift @path, $local_bin if -d $local_bin;
        bless { dir => $dir, home => $home, path => \@path }, $class;
    }

    sub home { $_[0]->{home} }

    sub dir { $_[0]->{dir} }

    sub platform {
        my $self = shift;
        return 'macos' if $^O eq 'darwin';
        return 'redhat' if -f '/etc/redhat-release';
        return 'debian' if -f '/etc/os-release';
        return 'unknown';
    }

    sub ln {
        my $self = shift;
        my $from = shift;
        my $to = shift || $from;
        $from = File::Spec->catfile($self->{dir}, $from) if !File::Spec->file_name_is_absolute($from);
        $to = File::Spec->catfile($self->{home}, $to) if !File::Spec->file_name_is_absolute($to);
        die "missing $from" if !-e $from;
        unlink $to;
        my $parent = File::Basename::dirname($to);
        File::Path::mkpath($parent) if !-d $parent;
        warn "ln $from $to\n";
        symlink $from, $to or die "symlink $from $to: $!";
    }

    sub run {
        my ($self, @cmd) = @_;
        local %ENV = %ENV;
        $ENV{PATH} = join ":", @{$self->{path}};
        warn "@cmd\n";
        !system @cmd or die "FAIL\n";
    }

    sub which {
        my ($self, $exe) = @_;
        my ($bin) = grep { -x $_ && !-d $_ } map { File::Spec->catfile($_, $exe) } @{$self->{path}};
        $bin;
    }

    sub homebrew_home {
        my $self = shift;
        return if $self->platform ne "macos";
        return "/opt/homebrew" if -d "/opt/homebrew";
        return "/usr/local";
    }

    sub git_version {
        my $self = shift;
        my $git = $self->which("git") or return 0;
        for (`$git --version`) {
            /git version (\d+)\.(\d+)\.(\d+)/ and return $1 * (1_000**2) + $2 * 1_000 + $3;
        }
        return 0;
    }

    sub vim_version {
        my $self = shift;
        my $vim = $self->which("vim") or return 0;
        for (`$vim --version`) {
            /VIM - Vi IMproved (\d+)\.(\d+)/ and return $1 * (1_000**2) + $2 * 1_000;
        }
        return 0;
    }
}

my $app = App->new;
sub ln { $app->ln(@_) }
sub run { $app->run(@_)  }
sub which ($) { $app->which(@_) }
sub mkdir_p ($) { File::Path::mkpath($_[0]) if !-d $_[0] }

my $dir = $app->dir;
my $home = $app->home;
my $platform = $app->platform;
my $homebrew_home = $app->homebrew_home;
my $vim_version = $app->vim_version;
my $git_version = $app->git_version;

ln ".aliases";
ln ".gitconfig";
ln ".gitignore";
ln ".spellunker.en";
ln ".vimrc";
ln ".zshenv";
ln ".zshrc";
ln ".replyrc";
ln ".dzil.config.ini" => ".dzil/config.ini";
ln ".dir_colors" if $platform eq "redhat";
ln $dir => "$home/dotfiles" if !-e "$home/dotfiles";

if (!-d "$home/.vim/autoload" && $vim_version >= 7003000 && $git_version) {
    run "git", "clone", "https://github.com/junegunn/vim-plug", "$home/.vim/plugged/vim-plug";
    ln "$home/.vim/plugged/vim-plug/plug.vim", "$home/.vim/autoload/plug.vim";
    run "vim", "--not-a-term", "-c", ":PlugInstall", "-c", ":q", "-c", ":q";
}

if ($homebrew_home) {
    my %file = (
        tar => "$homebrew_home/bin/gtar",
        date => "$homebrew_home/bin/gdate",
        ls => "$homebrew_home/bin/gls",
        xargs => "$homebrew_home/bin/gxargs",
        find => "$homebrew_home/bin/gfind",
    );
    for my $name (sort keys %file) {
        my $file = $file{$name};
        if (-f $file) {
            ln $file, "$home/bin/$name";
        } else {
            warn "SKIP link to ~/bin, because $file is missing\n";
        }
    }
}

if ($platform eq "macos") {
    mkdir_p "$home/bin";
    open my $fh, ">", "$home/bin/code" or die;
    say {$fh} q[#!/bin/bash];
    say {$fh} q[export JAVA_HOME=$HOME/env/java/versions/$(tinyenv java version)];
    say {$fh} q[exec open -b com.microsoft.VSCode "$@"];
    close $fh;
    chmod 0755, "$home/bin/code";
}

if (which "curl" && which "perl" && which "unzip") {
    run "$dir/install";
}
