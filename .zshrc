# zmodload zsh/zprof

fpath=(
  /usr/local/share/zsh-completions(N-/)
  /opt/homebrew/share/zsh-completions(N-/)
  $fpath
)

path=(
  ~/env/bin(N-/)
  ~/dotfiles/bin(N-)
  ~/bin(N-/)
  ~/local/bin(N-/)
  /usr/local/bin(N-/)
  /opt/homebrew/bin(N-/)
  /usr/bin(N-/)
  /bin(N-/)
  /usr/local/sbin(N-/)
  /opt/homebrew/sbin(N-/)
  /usr/sbin(N-/)
  /sbin(N-/)
  $path
)

# REPORTTIME=5
HISTFILE=~/.zsh_history
HISTSIZE=10000000
SAVEHIST=$HISTSIZE

# perl -le 'print "\e[1;${_}m$_\e[m" for 30..37'
___color() {
  echo -en "%{\e[${1}m%}${2}%{\e[m%}"
}
___git() {
  /usr/bin/perl ~/dotfiles/_bin/__git_branch_status.pl
}

HOST=$(hostname)
PROMPT="$(___color "0;1;33" $HOST) $(___color "0;1;34" %~)
\$(___git)❯ "

setopt auto_remove_slash
setopt extended_history hist_ignore_dups hist_ignore_space prompt_subst
setopt extended_glob list_types always_last_prompt
setopt cdable_vars sh_word_split auto_param_keys pushd_ignore_dups
setopt auto_pushd ignoreeof combining_chars no_flow_control
zstyle ':completing:*' format '%BCompleting %d%b'
zstyle ':completing:*' group-name
zstyle ':completion:*' list-colors 'di=1;34' 'ln=1;36' 'so=32' 'ex=1;32' 'bd=46;34' 'cd=43;34'

autoload -Uz compinit
for dump in ~/.zcompdump(N.mh+24); do
  compinit
  touch ~/.zcompdump
done
compinit -C

chpwd() {
  ls
}
cdup() {
  echo; cd .. >/dev/null; zle reset-prompt
}
zle -N cdup
bindkey -e
bindkey '^U' cdup
bindkey "\e[3~" delete-char
# bindkey '^?'    delete-char
# bindkey '^H'    delete-char

__print_known_hosts() {
  if [[ -n $_KNOWN_HOSTS_FILE ]]; then
    cat $_KNOWN_HOSTS_FILE
  elif [ -f $HOME/.ssh/known_hosts ]; then
    /usr/bin/perl -nle '@F = split /[ ,]/, $_; print $F[0]' $HOME/.ssh/known_hosts
  fi
}
_cache_hosts=($( __print_known_hosts ))

peco_src() {
  if ! type peco >/dev/null 2>&1; then
    return
  fi
  local selected_dir=$(/usr/bin/perl -E 'chdir shift; say for grep -d, glob "*/*/*"' ~/src | peco --query "$LBUFFER")
  if [ -n "$selected_dir" ]; then
    BUFFER="cd ~/src/${selected_dir}"
    zle accept-line
  fi
  zle clear-screen
}
zle -N peco_src
bindkey '^S' peco_src

chdir_weekdir() {
  local sunday=$(date -d "`date +%u` days ago" +%Y%m%d)
  local dir=$HOME/try/$sunday
  if [ ! -d $dir ]; then mkdir -p $dir; fi
  BUFFER="cd $dir"
  zle accept-line
  zle clear-screen
}
zle -N chdir_weekdir
bindkey '^W' chdir_weekdir

open_texteditor() {
  /usr/bin/perl ~/dotfiles/_bin/open-text-edit.pl
}
zle -N open_texteditor
bindkey '^T' open_texteditor

[[ -f $HOME/env/bin/tinyenv ]] && eval "$($HOME/env/bin/tinyenv zsh-completions)"

[ -f ~/.zshrc.local ] && source ~/.zshrc.local
source ~/.aliases

# zprof
