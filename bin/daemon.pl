#!/usr/bin/perl
use 5.008005;
use strict;
use warnings;
use POSIX ();
use Cwd ();

my @cmd = @ARGV;


my $logfile = $ENV{DAEMON_LOGFILE} || "daemon.@{[time]}.log";

if (fork) {
    sleep 1;
    my @line = do { open my $fh, "<", $logfile or die; <$fh> };
    print "logfile $logfile\n";
    print @line;
    STDOUT->flush;
    my $pid;
    for (@line) {
        if (/^pid\s*(\d+)/) {
            $pid = $1;
            last;
        }
    }
    die unless $pid;
    if (kill 0 => $pid) {
        print "\e[32mSuccessfully running, pid $pid\e[m\n";
        exit;
    } else {
        warn "\e[31mExit too early\e[m\n";
        exit 1;
    }
}
fork and exit;

POSIX::setsid();
open my $fh, ">>", $logfile or die;
print {$fh} "start ", scalar(localtime), "\n";
print {$fh} "cmd @cmd\n";
print {$fh} "dir ", Cwd::getcwd(), "\n";
print {$fh} "pid $$\n";
print {$fh} "\n";
close $fh;
open STDOUT, ">>", $logfile;
open STDERR, ">&", \*STDOUT;
open STDIN, "</dev/null";
exec {$cmd[0]} @cmd;
exit 255;
