#!/bin/bash

if ! type docker &>/dev/null; then
  echo 'Cannot find `docker` command' >&2
  exit 1
fi

DOCKER_PS=$(docker ps -a -q -f status=exited)
if [ -n "$DOCKER_PS" ]; then
  docker rm -f `docker ps -a -q -f status=exited`
fi

DOCKER_IMAGES=$(docker images -q -f dangling=true)
if [ -n "$DOCKER_IMAGES" ]; then
  docker rmi -f `docker images -q -f dangling=true`
fi
