#!/bin/bash
# shellcheck disable=SC2046

if ! type tinyenv &>/dev/null; then
  exit 1
fi

set -x

if [[ $(uname) = Darwin ]]; then
  export OPENSSL_PREFIX=$(brew --prefix openssl)
fi

ROOT=$HOME/env/perl
tinyenv perl install latest &>/dev/null
LATEST=$(tinyenv perl versions --bare | grep relocatable- | sort | tail -1)
if [[ -n $LATEST ]]; then
  rm -rf "$ROOT"/versions/"$LATEST"
fi
tinyenv perl install latest
rm -f "$ROOT"/versions/"$LATEST"/bin/perl5.*
touch "$ROOT"/versions/"$LATEST"/"$(date +'%Y-%m-%d')"
tinyenv perl global "$LATEST"

curl -fsSL --compressed https://raw.githubusercontent.com/skaji/cpm/main/cpm | \
  "$ROOT"/versions/"$LATEST"/bin/perl \
  - install -w8 -r Fixed,CLASS@1.00 -r 02packages -g $(cat ~/dotfiles/modules)

"$ROOT"/versions/"$LATEST"/bin/perl -nle \
  'm{,([^,]+)\| Unpacking } and push @a, $1 } { print " \e[1;32m$_\e[m" for sort @a' \
  ~/.perl-cpm/build.log

tinyenv perl rehash
