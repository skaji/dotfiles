#!/usr/bin/env perl
use v5.16;
use warnings;

use Encode ();

my $c = shift;
if (!$c || $c eq "-h" || $c eq "--help") {
    die "Usage: $0 CHAR\n";
}

my $point = ord Encode::decode_utf8 $c;
my $point_str = $point < 0xFFFF ? (sprintf "U+%04X", $point) : (sprintf "U+%X", $point);

printf "%s, https://www.compart.com/en/unicode/%s\n", $point_str, $point_str;
