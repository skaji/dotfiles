#!/bin/bash
# shellcheck disable=SC2046

if ! type tinyenv &>/dev/null; then
  exit 1
fi

set -x

if [[ $(uname) = Darwin ]]; then
  export OPENSSL_PREFIX=$(brew --prefix openssl)
fi

ROOT=$HOME/env/perl
LATEST=$(tinyenv perl versions --bare | perl -le 'chomp(@a = sort grep { /^5\.(\d{2})\.\d+$/ && $1 % 2 == 0 } <>); print $a[-1]')

if [[ -z $LATEST ]]; then
  echo "missing latest perl"
  exit 1
fi

tinyenv perl global "$LATEST"
tinyenv perl reset -
touch "$ROOT"/versions/"$LATEST"/$(date +%Y-%m-%d)

curl -fsSL --compressed https://raw.githubusercontent.com/skaji/cpm/main/cpm | \
  "$ROOT"/versions/"$LATEST"/bin/perl \
  - install -w8 -r Fixed,CLASS@1.00 -r 02packages -g $(cat ~/dotfiles/modules)

"$ROOT"/versions/"$LATEST"/bin/perl -nle \
  'm{,([^,]+)\| Unpacking } and push @a, $1 } { print " \e[1;32m$_\e[m" for sort @a' \
  ~/.perl-cpm/build.log

tinyenv perl rehash
