let mapleader = ","

let lsp_types = [ 'go', 'json', 'yaml', 'make', 'dockerfile' ]
let ale_types = [ 'perl' ]

call plug#begin('~/.vim/plugged')

Plug 'dense-analysis/ale', { 'for': ale_types }
Plug 'skaji/syntax-check-perl', { 'for': ale_types }

Plug 'mattn/vim-goimports', { 'for': lsp_types }
Plug 'mattn/vim-lsp-settings', { 'for': lsp_types }
Plug 'prabirshrestha/asyncomplete-lsp.vim', { 'for': lsp_types }
Plug 'prabirshrestha/asyncomplete.vim', { 'for': lsp_types }
" Plug 'prabirshrestha/vim-lsp', { 'for': lsp_types }
Plug 'skaji/vim-lsp', { 'for': lsp_types, 'branch': 'skaji' }

Plug 'ConradIrwin/vim-bracketed-paste'
Plug 'itchyny/lightline.vim'
Plug 'junegunn/vim-plug'
Plug 'skaji/sahara', { 'branch': 'markus1189' }
Plug 'skaji/vim-block-comment'
Plug 'skaji/vim-bufnewfile-template'

" Plug 'tpope/vim-vinegar'

call plug#end()

if filereadable(expand('~/.vim/plugged/sahara/colors/sahara.vim'))
  colorscheme sahara
endif

imap ^[OA <Up>
imap ^[OB <Down>
imap ^[OC <Right>
imap ^[OD <Left>
nmap <plug>() <Plug>(lsp-float-close)

" map <A-s> <C-w><C-s>

" runtime/syntax/go.vim
let g:go_highlight_array_whitespace_error = 1
let g:go_highlight_chan_whitespace_error = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_space_tab_error = 1
let g:go_highlight_trailing_whitespace_error = 1
let g:go_highlight_operators = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_parameters = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_fields = 1
let g:go_highlight_types = 1
let g:go_highlight_build_constraints = 1
let g:go_highlight_string_spellcheck = 1
let g:go_highlight_format_strings = 1
let g:go_highlight_generate_tags = 1
let g:go_highlight_variable_assignments = 1
let g:go_highlight_variable_declarations = 1

let g:goimports_simplify = 1
let g:goimports_simplify_cmd = 'gofumpt'

" runtime/syntax/perl.vim
let g:perl_sub_signatures = 1

" ale
let g:ale_fix_on_save = 1
let g:ale_lint_on_text_changed = 'always'
let g:ale_linters = {}
let g:ale_linters.perl = ['syntax-check']
let g:ale_fixers = {}
let g:ale_perl_syntax_check_config = expand('~/dotfiles/.syntax-check-perl.pl')
autocmd filetype perl nmap <silent> <C-j> <Plug>(ale_next_wrap)

" lsp
function! s:on_lsp_buffer_enabled() abort
  setlocal omnifunc=lsp#complete
  setlocal signcolumn=yes
  if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
  nmap <buffer> gd <plug>(lsp-definition)
  nmap <buffer> gs <plug>(lsp-document-symbol-search)
  nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
  nmap <buffer> gr <plug>(lsp-references)
  nmap <buffer> gi <plug>(lsp-implementation)
  nmap <buffer> gt <plug>(lsp-type-definition)
  nmap <buffer> <leader>rn <plug>(lsp-rename)
  nmap <buffer> [g <plug>(lsp-previous-diagnostic)
  nmap <buffer> ]g <plug>(lsp-next-diagnostic)
  nmap <buffer> K <plug>(lsp-hover)
  " nnoremap <buffer> <expr><c-f> lsp#scroll(+4)
  " nnoremap <buffer> <expr><c-d> lsp#scroll(-4)

  " let g:lsp_format_sync_timeout = 1000
  " autocmd! BufWritePre *.rs,*.go call execute('LspDocumentFormatSync')
  " refer to doc to add more commands

  " let g:lsp_inlay_hints_enabled = 1
  let g:lsp_diagnostics_enabled = 1
  let g:lsp_diagnostics_echo_cursor = 1
  let g:asyncomplete_popup_delay = 200
  inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
  inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
  inoremap <expr> <cr>    pumvisible() ? asyncomplete#close_popup() : "\<cr>"
  nmap <silent> [ :LspRename<CR>
  nmap <silent> ] :LspHover<CR>
  nmap <silent> <C-j> :LspNextError<CR>
  nmap <silent> <C-u> <C-o>

  set updatetime=750
  autocmd CursorHold * :LspHover

  let g:lsp_float_max_width = 0
endfunction

augroup lsp_install
  au!
  " call s:on_lsp_buffer_enabled only for languages that has the server registered.
  autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END

" check:   locale -a
" install: apt-get install locales-all
language messages en_US.UTF-8

set backspace=indent,eol,start
set clipboard+=unnamed
set cursorline
set enc=utf-8
set expandtab
set hidden
set hlsearch
set laststatus=2
set list
set listchars=tab:.\ ,extends:>,precedes:<,trail:-
set matchtime=3
set mmp=5000
set modeline
set mouse-=a " a = all, i = insert, v = visual, n = normal
set nobackup
set nofixeol
set nofoldenable
set noshowmode
set noswapfile
set noundofile
set novisualbell
set nowrap
set nowritebackup
set number
set pastetoggle=<F9>
set scrolloff=999
set shiftround
set shiftwidth=2
set showcmd
set showmatch
set smartcase
set softtabstop=2
set switchbuf=useopen
set t_Co=256
" set t_kD=<ESC>[3~
" set t_kD=^H
set t_kD=[3~
set tabstop=8
set textwidth=0
set updatetime=500
set vb t_vb=
set viminfo='50,\"30000

if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
endif

syntax on
highlight ZenkakuSpace ctermbg=white
match ZenkakuSpace /　/

autocmd FileType perl,raku,perl6,markdown
  \ setlocal expandtab shiftwidth=4 softtabstop=4 tabstop=8
autocmd FileType go,gomod,make
  \ setlocal noexpandtab shiftwidth=4 softtabstop=4 tabstop=4
autocmd BufNewFile,BufRead *.psgi,cpanfile
  \ setlocal filetype=perl
autocmd BufNewFile,BufRead *.tmpl,*.gotmpl
  \ setlocal filetype=gotexttmpl
autocmd BufNewFile,BufRead go.mod
  \ setlocal filetype=gomod
autocmd BufNewFile,BufRead go.sum
  \ setlocal filetype=gosum
autocmd BufRead,BufNewFile *.graphql,*.graphqls,*.gql
  \ setlocal filetype=graphql

autocmd Filetype go,gomod,make :highlight SpecialKey term=NONE cterm=NONE ctermfg=237 ctermbg=NONE gui=NONE guifg=#87d700 guibg=NONE
autocmd FileType go :highlight goErr cterm=bold ctermfg=214
autocmd FileType go :match goErr /\<err\>/
