#!/bin/bash

set -euxo pipefail

env DEBIAN_FRONTEND=noninteractive apt-get update
env DEBIAN_FRONTEND=noninteractive apt-get install -y \
  build-essential \
  curl \
  dnsutils \
  dstat \
  git \
  less \
  libbz2-dev \
  libncurses5-dev \
  libreadline-dev \
  libsqlite3-dev \
  libssl-dev \
  locales-all \
  net-tools \
  perl \
  procps \
  sudo \
  tzdata \
  unzip \
  vim \
  wget \
  xz-utils \
  zlib1g-dev \
  zsh \
  ;

echo Asia/Tokyo > /etc/timezone
rm -f /etc/localtime
dpkg-reconfigure -f noninteractive tzdata

useradd --create-home --groups sudo --shell /bin/zsh skaji
echo skaji:skaji | chpasswd

echo '* soft nofile 1048576' >> /etc/security/limits.conf
echo '* hard nofile 1048576' >> /etc/security/limits.conf

# install -o skaji -g skaji -m 0700 -d /home/skaji/.ssh
# curl https://github.com/skaji.keys > /home/skaji/.ssh/authorized_keys
# chown skaji:skaji /home/skaji/.ssh/authorized_keys
# chmod 0400 /home/skaji/.ssh/authorized_keys

# su -l skaji -c '
# git clone https://bitbucket.org/skaji/dotfiles.git ~/src/bitbucket.org/skaji/dotfiles
# perl ~/src/bitbucket.org/skaji/dotfiles/setup.pl
# '

# curl -fsSL https://github.com/krallin/tini/releases/download/v0.19.0/tini -o /sbin/tini
# chmod +x /sbin/tini
