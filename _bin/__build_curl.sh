#!/bin/bash

# For RPATH, see https://cmake.org/Wiki/CMake_RPATH_handling

# prereq:
#  cmake
#  ninja
#  openssl

TARGET=${1-$HOME/local}

if [[ $(uname) = Darwin ]]; then
  _CMAKE_INSTALL_RPATH='@executable_path/../lib'
else
  _CMAKE_INSTALL_RPATH='$ORIGIN/../lib'
fi

if [[ -e /usr/local/opt/openssl@1.1 ]]; then
  _CONFIGURE_OPT_OPENSSL_ROOT_DIR="\
    -DOPENSSL_ROOT_DIR=/usr/local/opt/openssl@1.1 \
"
  _CONFIGURE_OPT_CURL_OPENSSL="\
    -DCURL_CA_BUNDLE=/usr/local/etc/openssl@1.1/cert.pem \
    -DCURL_CA_PATH=/usr/local/etc/openssl@1.1/certs \
"
else
  if [[ ! -e $TARGET/lib/libssl.so ]]; then
    set -ex
    $(dirname $0)/__build_openssl.sh
    set +ex
  fi
  _CONFIGURE_OPT_OPENSSL_ROOT_DIR="\
    -DOPENSSL_ROOT_DIR=$TARGET \
"
fi

_CONFIGURE_OPT="\
  -G Ninja \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_INSTALL_PREFIX=$TARGET \
  -DCMAKE_INSTALL_LIBDIR=$TARGET/lib \
  -DCMAKE_INSTALL_RPATH=$_CMAKE_INSTALL_RPATH \
  -DCMAKE_MACOSX_RPATH=ON \
"
_INSTALL_OPT="\
  --build . \
  --config Release \
  --target install \
"

set -ex

if [[ ! -d ~/src/github.com/nghttp2/nghttp2 ]]; then
  git clone https://github.com/nghttp2/nghttp2 ~/src/github.com/nghttp2/nghttp2
fi
cd ~/src/github.com/nghttp2/nghttp2
git clean -xfd
git checkout .
git pull
mkdir out
cd out
cmake $_CONFIGURE_OPT $_CONFIGURE_OPT_OPENSSL_ROOT_DIR -DENABLE_LIB_ONLY=ON ..
cmake $_INSTALL_OPT

if [[ ! -d ~/src/github.com/google/brotli ]]; then
  git clone https://github.com/google/brotli ~/src/github.com/google/brotli
fi
cd ~/src/github.com/google/brotli
git clean -xfd
git checkout .
git pull
mkdir out
cd out
cmake $_CONFIGURE_OPT ..
cmake $_INSTALL_OPT

if [[ ! -d ~/src/github.com/curl/curl ]]; then
  git clone https://github.com/curl/curl ~/src/github.com/curl/curl
fi
cd ~/src/github.com/curl/curl
git clean -xfd
git checkout .
git pull
perl -i -pe 's/\Qfind_package(BROTLI QUIET)/find_package(Brotli REQUIRED)/' CMakeLists.txt
mkdir out
cd out

_CURL_CONFIGURE_OPT="\
  -DCURL_BROTLI=ON -DUSE_NGHTTP2=ON -DBUILD_SHARED_LIBS=OFF $_CONFIGURE_OPT_OPENSSL_ROOT_DIR $_CONFIGURE_OPT_CURL_OPENSSL"
cmake $_CONFIGURE_OPT $_CURL_CONFIGURE_OPT ..
cmake $_INSTALL_OPT
