#!/bin/bash

_PREFIX=${1-$HOME/local}
_TARGET=linux-x86_64
_OPENSSL_VERSION=1.1.1c
_DIR=$(mktemp -d /tmp/openssl-build.XXXXXXXXXX)
if [ -n "$_NO_SHARED" ]; then
  _NO_SHARED=no-shared
fi

set -ex

cd $_DIR
wget -q https://www.openssl.org/source/openssl-$_OPENSSL_VERSION.tar.gz
tar xzf openssl-$_OPENSSL_VERSION.tar.gz
cd openssl-$_OPENSSL_VERSION

/usr/bin/perl Configure \
  --prefix=$_PREFIX \
  --libdir=$_PREFIX/lib \
  '-Wl,-rpath,$(LIBRPATH)' \
  no-ssl3 no-ssl3-method no-zlib $_NO_SHARED \
  $_TARGET

make -j8
make install_sw
cd /
rm -rf $_DIR
