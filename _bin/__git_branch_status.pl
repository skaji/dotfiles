#!/usr/bin/env perl
use strict;
use warnings;

sub green ($) { "%{\e[32m%}$_[0]%{\e[m%}" }
sub pink  ($) { "%{\e[31m%}$_[0]%{\e[m%}" }
sub white ($) { "%{\e[1m%}$_[0]%{\e[m%}" }

if (my $env = $ENV{ZSH_SKIP_GIT_STATUS}) {
    my @dir = split /,/, $env;
    require Cwd;
    my $cwd = Cwd::getcwd();
    if (grep { $cwd =~ $_ } @dir) {
        exit;
    }
}

my @line = `git branch 2>/dev/null`;
exit if $?;

my $br;
for (@line) {
    if (/^\* \(?([^\n\)]+)/) {
        $br = white $1;
        last;
    }
}

my %h;
for (`git status -s 2>/dev/null`) {
    if (/^(.)(.)/) {
        if ($1 eq "?") {
            $h{ pink "?" }++;
            next;
        }
        $h{ green $1 }++ if $1 ne " ";
        $h{ pink $2  }++ if $2 ne " ";
    }
}

print $br ? "$br " : "", %h ? (join "", sort keys %h) . " " : "";
