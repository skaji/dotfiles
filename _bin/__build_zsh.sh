#!/bin/bash

VERSION=5.6.2
URL=https://www.zsh.org/pub/zsh-$VERSION.tar.xz
_TEMPDIR=$(mktemp -d)

zsh_install() {
  (
    set -ex
    wget -q $URL
    tar xJf zsh-$VERSION.tar.xz
    cd zsh-$VERSION
    ./configure --without-tcsetpgrp --prefix=$HOME/local
    make --jobs=4 install
  )
}

set -x
cd $_TEMPDIR
zsh_install
EXIT=$?
cd /
rm -rf $_TEMPDIR
exit $EXIT
