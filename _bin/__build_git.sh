#!/bin/bash

PREFIX=$HOME/local
VERSION=2.21.0
URL=https://mirrors.edge.kernel.org/pub/software/scm/git/git-$VERSION.tar.xz
_TEMPDIR=$(mktemp -d)

init() {
  (
    set -ex
    which msgfmt
  ) || exit 1
}

git_install() {
  (
    set -ex
    wget -q $URL
    tar xJf git-$VERSION.tar.xz
    cd git-$VERSION
    env PATH=/usr/bin:$PATH ./configure --prefix=$PREFIX --with-curl --with-expat --without-tcltk
    make --jobs=4 NO_INSTALL_HARDLINKS=1 install
    ( cd contrib/subtree && make prefix=$PREFIX install )
  )
}

set -x
init
cd $_TEMPDIR
git_install
EXIT=$?
cd /
rm -rf $_TEMPDIR
exit $EXIT
