#!/usr/bin/env perl
use 5.28.1;
use warnings;
use File::Temp ();
use File::pushd ();
use Path::Tiny ();
use App::cpm::CLI;
use App::FatPacker::Simple;
sub cmd { warn "=> @_\n"; !system @_ or die "=> FAIL @_\n" }

my $dir = "$ENV{HOME}/src/github.com/miyagawa/cpanminus";
my $out = "$ENV{HOME}/bin/cpanm_";
die "miss $dir" unless -d $dir;

my $tempdir = File::Temp::tempdir(CLEANUP => 1);

my $content = <<'___';
#!/usr/bin/env perl
use strict;
use App::cpanminus::script;
my $app = App::cpanminus::script->new(name => "App::cpanminus");
$app->parse_options(@ARGV);
exit $app->doit;
___

cmd "git", "clone", $dir, "$tempdir/cpanminus";
{
    my $guard = File::pushd::pushd("$tempdir/cpanminus/App-cpanminus");
    cmd "git", "checkout", "devel";
    my $script = Path::Tiny->new("cpanm_");
    $script->spew($content);
    $script->chmod(0755);
    warn "=> Installing Module::CoreList\n";
    App::cpm::CLI->new->run("install", "Module::CoreList");
    warn "=> FatPacking $out\n";
    App::FatPacker::Simple->new->parse_options(
        "-d", "lib,local/lib/perl5,fatlib,../Menlo-Legacy/lib,../Menlo/lib",
        "-o", $out,
        "--cache", "$ENV{HOME}/.perl-cpm/.fatpack-cache",
        "cpanm_",
    )->run;
}
