#!/bin/bash

COMMIT=${1:-master}
DIR=$(cd $(dirname $0) >/dev/null 2>&1; pwd)

set -ex

if [[ ! -d ~/src/github.com/vim/vim ]]; then
  git clone https://github.com/vim/vim ~/src/github.com/vim/vim
fi

if [[ ! -f ~/local/bin/lua ]]; then
  echo "Need __build_lua.sh first"
  exit 1
fi

cd ~/src/github.com/vim/vim
git clean -xffd
git checkout .
git checkout master
git pull
git checkout $COMMIT

for f in gohtmltmpl.vim gotexttmpl.vim gomod.vim gosum.vim; do
  curl -fsSL https://raw.githubusercontent.com/fatih/vim-go/master/syntax/$f \
    -o runtime/syntax/$f
done
curl -fsSL https://raw.githubusercontent.com/jparise/vim-graphql/master/syntax/graphql.vim \
  -o runtime/syntax/graphql.vim
curl -fsSL https://raw.githubusercontent.com/vim-perl/vim-perl/dev/indent/perl.vim \
  -o runtime/indent/perl.vim

./configure --prefix=$HOME/local --with-lua-prefix=$HOME/local --enable-luainterp=yes
rm -f $HOME/local/bin/{ex,rview,rvim,view,vimdiff}
make --jobs=4 install
