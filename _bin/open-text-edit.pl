#!/usr/bin/env perl
use v5.34;
use warnings;

use Fcntl ();
use File::Path ();
use POSIX ();

if ($^O ne "darwin") {
    die "only macos\n";
}

my $now = time;
my $wday = (localtime $now)[6]; # 0:Sun, 1:Mon, 2:Tue, ..., 7:Sat
my $sunday = POSIX::strftime("%Y%m%d", localtime($now - $wday * 24 * 60 * 60));

my $dir = "$ENV{HOME}/try/$sunday";
if (!-d $dir) {
    File::Path::make_path($dir);
}
opendir my $dh, $dir or die;
my @text = sort grep { /^memo_\d{3}.txt$/ } readdir $dh;
closedir $dh;

my $max = 0;
if (@text) {
    ($max) = $text[-1] =~ /memo_(\d{3})/;
}
my $new = sprintf "%s/memo_%03d.txt", $dir, $max + 1;
my $flag = Fcntl::O_CREAT | Fcntl::O_EXCL | Fcntl::O_RDONLY;
sysopen my $fh, $new, $flag or die "$!: $new";
close $fh;

exec "open", "-a", "TextEdit", $new;
