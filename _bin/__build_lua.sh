#!/bin/bash

set -eux

VERSION=5.4.7
INSTALL_TOP=$HOME/local

cd /tmp

curl -fsSL -O https://www.lua.org/ftp/lua-$VERSION.tar.gz
tar zxf lua-$VERSION.tar.gz
cd lua-$VERSION
make INSTALL_TOP=$INSTALL_TOP all install
