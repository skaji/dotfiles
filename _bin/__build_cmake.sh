#!/bin/bash

set -euxo pipefail

main() {
  local target=$HOME/local

  if [[ -z ${FORCE-} ]]; then
    if [[ -f $target/bin/cmake ]]; then
      echo "already exists $target/bin/cmake, try again with FORCE=1"
      exit 1
    fi
  fi
  mkdir -p $target/bin $target/share

  local url
  local os=$(uname -s)
  local arch=$(uname -m)
  local version=3.31.0
  local tempdir=$(mktemp -d)

  if [[ $os == Darwin ]]; then
    url=https://github.com/Kitware/CMake/releases/download/v$version/cmake-$version-macos-universal.tar.gz
    curl -fsSL $url | tar xzf - -C $tempdir --strip-components=1
    cp -f $tempdir/CMake.app/Contents/bin/* $target/bin
    rm -rf $target/share/cmake-*
    mv $tempdir/CMake.app/Contents/share/cmake-* $target/share/
  else
    if [[ $arch == x86_64 ]]; then
      url=https://github.com/Kitware/CMake/releases/download/v$version/cmake-$version-linux-x86_64.tar.gz
    else
      url=https://github.com/Kitware/CMake/releases/download/v$version/cmake-$version-linux-aarch64.tar.gz
    fi
    curl -fsSL $url | tar xzf - -C $tempdir --strip-components=1
    cp -f $tempdir/bin/* $target/bin/
    rm -rf $target/share/cmake-*
    mv $tempdir/share/cmake-* $target/share/
  fi
  rm -rf $tempdir
}

main
