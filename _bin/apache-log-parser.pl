#!/usr/bin/env perl
use strict;
use warnings;

my @FIELD = qw( ip - user time req code size ref ua );
my %FIELD = map { $_ => 1 } @FIELD;
my @want  = grep { exists $FIELD{$_} || help() } split /,/, shift or help();

while (<STDIN>) {
    chomp;
    my %h;
    @h{ @FIELD } = $_ =~
        m!^([^\s]*) ([^\s]*) ([^\s]*) \[([^]]*)\] "([^"]*)" ([^\s]*) ([^\s]*) "([^"]*)" "([^"]*)"!;
    print join("\t", map { $h{$_} } @want), "\n";
}

sub help {
    print <<"HELP";
usage:
    cat access.log | $0 ip,ua,ref
available field:
    @FIELD
HELP
    exit;
}
