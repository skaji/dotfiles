use strict;
use warnings;

my $FILENAME = $ENV{PERL_SYNTAX_CHECK_FILENAME} || "";
my $OFF = $ENV{Y};

my $MINE = 0;
if ($FILENAME =~ m{try/\d{8}/} || $FILENAME =~ m{^$ENV{HOME}/[^/]+$}) {
    $MINE = 1;
} elsif (my ($user) = $FILENAME =~ m{src/[^/]+/([^/]+)/}) {
    if ($user eq $ENV{USER}) {
        $MINE = 1;
    }
}

my $new_check = do {
    my (@required_module, $is_fatpacked, $seen_END);
    my $MODULE = qr/[a-zA-Z0-9_:]+/o;
    my %fix = (
        # Getopt-Long-2.55 has Getopt::Long::Parser as a separated package
        # note that perl v5.40 comes with Getopt-Long-2.57
        # 'Getopt::Long::Parser' => 'Getopt::Long',
    );
    sub {
        my ($line, $filename, $lines) = @_;
        return if $is_fatpacked || $seen_END;
        if ($line =~ /^# This chunk of stuff was generated by App::FatPacker/) {
            $is_fatpacked++;
            return;
        }
        return if $line =~ /^\s*#/;
        if ($line eq "__END__" || $line eq "__DATA__") {
            $seen_END++;
            return;
        }
        if (my ($found) = $line =~ /(?:^|\s)($MODULE)->/o) {
            return if grep { $found eq $_ } qw(shift __PACKAGE__ STDOUT STDERR STDIN);
            if (!@required_module) {
                for my $i (0 .. $#{$lines}) {
                    my $l = $lines->[$i];
                    last if $l eq "__END__" || $l eq "__DATA__";
                    if (my @m = $l =~ /\b(?:use|require)\s+($MODULE)/go) {
                        push @required_module, @m;
                    }
                    if ($l =~ /^\s*class\s+($MODULE)/o) {
                        push @required_module, $1;
                    }
                    if ($l =~ /^\s*package/) {
                        if ($l =~ /^\s*package\s+($MODULE)/o) {
                            push @required_module, $1;
                        } elsif ($l =~ /^\s*package\s*$/) {
                            if ($lines->[$i+1] =~ /^\s*($MODULE)/o) {
                                push @required_module, $1;
                            }
                        }
                    }
                }
            }
            $found = $fix{$found} if exists $fix{$found};
            if (!grep { $_ eq $found } @required_module) {
                return "miss use $found";
            }
        }
        return;
    };
};

my @use_module;
push @use_module, ['feature', '-M-feature=indirect'] if $] >= 5.032;

my $config = {
    compile => {
        skip => [
            (
                qr/Indirect call of method "VERSION" on object "version"/,
            ),
            !$MINE
                ? (qr/^Subroutine \S+ redefined/,
                   qr/^Name "\S+" used only once/,
                  )
                : (),
            $FILENAME =~ /\.psgi$/
                ? (qr/^Useless use of single ref constructor in void context/)
                : (),
        ],
        use_module => \@use_module,
    },
    regexp => {
        check => [
            qr/^ \s* my \s* \( .* , .* \) \s* = \s* shift/x,
            qr/pakcage/, # no syntax check
        ],
    },
    custom => {
        check => [
            $MINE && !$OFF ? $new_check : (),
        ]
    },
};
