#!/bin/bash

brew install \
  coreutils \
  findutils \
  gnu-tar \
  openssl \
  pstree \
  readline \
  tree \
  wget \
  xz \
  ;
